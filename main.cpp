#include <Arduino.h>
#include "SSD1306.h"
#include <WiFiClientSecure.h>
#include <ArduinoJson.h>
#include "time.h"
#include "header.h"
WiFiClientSecure client;

const char* ssid     = "";     // your network SSID (name of wifi network)
const char* password = ""; // your network password
const char*  server = "tradeogre.com";  // Server URL
uint8_t ledPin = 16; // Onboard LED reference
SSD1306 display(0x3c, 5, 4);
const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 19800;
const int   daylightOffset_sec = 0;

double nerva_price=0;
double nerva_volume=0;
double nerva_initprice=0;
double nerva_highprice=0;
struct tm timeinfo;
char buff[100];
char _timestamp[50];

void drawImageDemo() {
    // see http://blog.squix.org/2015/05/esp8266-nodemcu-how-to-create-xbm.html
    // on how to create xbm files
    display.drawXbm(64, 0, nerva_width, nerva_height, nerva_bits);

}

void printLocalTime()
{
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  strftime(_timestamp, sizeof(_timestamp), "%m/%d %H:%M:%S", &timeinfo);
  //sprintf(buff,"%A, %B %d %Y %H:%M:%S",&timeinfo);
  

}

void updateNervaPrice(){
  DynamicJsonDocument doc(2048);
  char json[2048];
  int index=0;
  Serial.println("\nStarting connection to server...");
  if (!client.connect(server, 443))
    Serial.println("Connection failed!");
  else {
    Serial.println("Connected to server!");
    // Make a HTTP request:
    client.println("GET https://tradeogre.com/api/v1/ticker/BTC-XNV HTTP/1.0");
    client.println("Host: tradeogre.com");
    client.println("Connection: close");
    client.println();

    while (client.connected()) {
      String line = client.readStringUntil('\n');
      if (line == "\r") {
        Serial.println("headers received");
        break;
      }
    }
    // if there are incoming bytes available
    // from the server, read them and print them:
    while (client.available()) {
      char c = client.read();
      Serial.write(c);
      json[index]=c;
      index++; 
    }

    client.stop();
    DeserializationError error = deserializeJson(doc, json);
    if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.c_str());
      return;
    }
    const char* price_str=doc["price"] ;
    const char* volume_str=doc["volume"] ;
    const char* initprice_str=doc["initialprice"] ;
    const char* highprice_str=doc["high"] ;

    nerva_price=atof(price_str)*100000000;
    nerva_volume=atof(volume_str);
    nerva_initprice=atof(initprice_str)*100000000;
    nerva_highprice=atof(highprice_str)*100000000;
  }
}


void setup() {
    pinMode(ledPin, OUTPUT);

    Serial.begin(115200);
    display.init(); // initialise the OLED
    //display.flipScreenVertically(); // does what is says
    display.setFont(ArialMT_Plain_10); // does what is says
    // Set the origin of text to top left
    display.setTextAlignment(TEXT_ALIGN_LEFT);

    digitalWrite(ledPin,LOW);
    WiFi.begin(ssid, password);
    WiFi.mode(WIFI_STA);

    // attempt to connect to Wifi network:
    while (WiFi.status() != WL_CONNECTED) {
      //Serial.print(".");
      // wait 1 second for re-trying
      delay(500);
    }
    display.clear();
    display.drawString(0, 0, "Connected to ");
    display.drawString(0, 10, ssid);
    display.display();
    Serial.print("Connected to ");
    Serial.println(ssid);
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
    //client.setCACert(caCert);
    WiFi.disconnect(true);
    digitalWrite(ledPin,HIGH);
  
}

void loop() {
  static unsigned long timer_data=-30000;
  static unsigned long timer_tick=0;

  if(millis()-timer_data>20000){
    WiFi.begin(ssid, password);
    WiFi.mode(WIFI_STA);
    WiFi.reconnect();
    digitalWrite(ledPin,LOW);
    delay(500);
    while (WiFi.status() != WL_CONNECTED) {
      //Serial.print(".");
      delay(500);
    }
    updateNervaPrice();
    timer_data=millis();
    WiFi.disconnect(true);
    WiFi.mode(WIFI_OFF);
    digitalWrite(ledPin,HIGH);

  }

  if(millis()-timer_tick>1000){
    printLocalTime();
    display.clear();
    display.drawString(0, 0, _timestamp);
    
    display.drawString(0, 10, "Init: "+String(nerva_initprice,0));
    display.drawString(0, 20, "High: "+String(nerva_highprice,0));
    display.drawString(0, 30, "Vol:"+String(nerva_volume,3)+ "BTC");
    display.drawString(0, 40, "Val: "+String(nerva_price,0));
    drawImageDemo();
    
    
    display.display();
    timer_tick=millis();
  }

    
    
    
}